﻿using System.Threading.Tasks;
using System.Collections.Generic;

using Atlassian.Jira;

namespace JIRAIssue.App.Model
{
    public class JiraApi : IJiraApi
    {
        private Jira Jira;

        public JiraApi(string jiraServer, string jiraLogin, string jiraPassword)
        {
            Jira = Jira.CreateRestClient(jiraServer, jiraLogin, jiraPassword);
        }

        public async Task<List<JiraIssue>> GetIssueAsync(List<string> IDIssue)
        {
            IDictionary<string, Issue> issue = await Jira.Issues.GetIssuesAsync(IDIssue);

            return GetField(issue);
        }

        private List<JiraIssue> GetField(IDictionary<string, Issue> issue)
        {
            List<JiraIssue> result = new List<JiraIssue>();

            foreach (var item in issue)
            {
                result.Add(new JiraIssue
                {
                    IssueId       = item.Value.Key.Value,
                    IssueType     = item.Value.Type.Name,
                    IssueCreated  = item.Value.Created.ToString(),
                    IssueDueDate  = item.Value.DueDate.ToString(),
                    IssueSummary  = item.Value.Summary,
                    IssueEpicLink = item.Value["Epic Link"].Value
                });
            }

            return result;
        }
    }
}