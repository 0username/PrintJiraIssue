﻿using System.Threading.Tasks;
using System.Collections.Generic;

namespace JIRAIssue.App.Model
{
    interface IJiraApi
    {
        Task<List<JiraIssue>> GetIssueAsync(List<string> IDIssue);
    }
}