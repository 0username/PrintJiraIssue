﻿namespace JIRAIssue.App.Model
{
    public class JiraIssue
    {
        public string IssueId       { get; set; }
        public string IssueType     { get; set; }
        public string IssueCreated  { get; set; }
        public string IssueDueDate  { get; set; }
        public string IssueSummary  { get; set; }
        public string IssueEpicLink { get; set; }
    }
}