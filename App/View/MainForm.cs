﻿using System;
using System.Windows.Forms;
using System.ComponentModel;
using System.Collections.Generic;

using JIRAIssue.Library.Print;
using System.Linq;

namespace JIRAIssue.App.View
{
    public partial class MainForm : Form, IMainForm
    {
        public event PrintEventHandler EventSetPrinter;
        public event PrintEventHandler EventGetPrinter;
        public event IssueEventHandler EventFetchIssue;

        private BindingList<Issue> Issue = new BindingList<Issue>();

        public MainForm()
        {
            InitializeComponent();

            DataGridIssue.DataSource = Issue;

            WebBrowser.Url = new Uri(Application.StartupPath + Properties.Settings.Default.PathLogo);
        }

        public void InitPresenter()
        {
            EventGetPrinter(null);
        }

        public void SetPrinter(List<Printer> printer)
        {
            ComboBoxPrinter.DataSource    = printer;
            ComboBoxPrinter.DisplayMember = "FullName";
            ComboBoxPrinter.ValueMember   = "Name";
        }

        private void SelectPrinter(object sender, EventArgs args)
        {
            EventSetPrinter(ComboBoxPrinter.Text);
        }

        private void SelectionDataGrid(object sender, EventArgs args)
        {
            DataGridIssue.AllowUserToAddRows = Issue.Count <= 20 ? true : false;
        }

        private void DeleteRowDataGrid(object sender, KeyEventArgs args)
        {
            if (args.KeyCode == Keys.Delete && Issue.Count > 0) Issue.RemoveAt(DataGridIssue.CurrentCell.RowIndex);
        }

        private void FetchIssue(object sender, EventArgs args)
        {
            EventFetchIssue(Issue.Select(issue => issue.IssueId).ToList());
        }

        private void PrintIssue(object sender, EventArgs args)
        {
            WebBrowser.Print();
        }

        public void ShowIssue(string issue)
        {
            WebBrowser.DocumentText = issue;
        }
    }
}