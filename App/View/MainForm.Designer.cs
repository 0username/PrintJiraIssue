﻿using System.Windows.Forms;
using System.ComponentModel;

namespace JIRAIssue.App.View
{
    partial class MainForm
    {
        private IContainer Components = null;

        private WebBrowser WebBrowser;

        private Label Label;

        private DataGridView DataGridIssue;

        private Button ButtonFetchIssue;
        private Button ButtonPrintIssue;

        private ComboBox ComboBoxPrinter;

        protected override void Dispose(bool disposing)
        {
            if (disposing && (Components != null)) Components.Dispose();

            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));

            this.WebBrowser       = new System.Windows.Forms.WebBrowser  ();
            this.Label            = new System.Windows.Forms.Label       ();
            this.DataGridIssue    = new System.Windows.Forms.DataGridView();
            this.ButtonFetchIssue = new System.Windows.Forms.Button      ();
            this.ButtonPrintIssue = new System.Windows.Forms.Button      ();
            this.ComboBoxPrinter  = new System.Windows.Forms.ComboBox    ();
            this.SuspendLayout();

            ((System.ComponentModel.ISupportInitialize)(this.DataGridIssue)).BeginInit();

            /**
             * WebBrowser
             */
            this.WebBrowser.Location = new System.Drawing.Point(10 , 10 );
            this.WebBrowser.Size     = new System.Drawing.Size (490, 300);
            this.WebBrowser.Name     = "WebBrowser";
            this.WebBrowser.WebBrowserShortcutsEnabled     = false;
            this.WebBrowser.IsWebBrowserContextMenuEnabled = false;

            /**
             * Label
             */
            this.Label.Location = new System.Drawing.Point(545, 10);
            this.Label.Size     = new System.Drawing.Size (95 , 15);
            this.Label.Name     = "Label"              ;
            this.Label.Text     = "Enter the issue ID:";
            this.Label.AutoSize = true;

            /**
             * DataGridIssue
             */
            this.DataGridIssue.Location = new System.Drawing.Point(515, 30 );
            this.DataGridIssue.Size     = new System.Drawing.Size (155, 220);
            this.DataGridIssue.Name     = "DataGridIssue";
            this.DataGridIssue.ColumnHeadersVisible     = false;
            this.DataGridIssue.RowHeadersVisible        = false;
            this.DataGridIssue.AllowUserToResizeColumns = false;
            this.DataGridIssue.AllowUserToResizeRows    = false;
            this.DataGridIssue.AutoSizeColumnsMode         = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DataGridIssue.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridIssue.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.DataGridIssue.SelectionChanged += new System.EventHandler                 (this.SelectionDataGrid);
            this.DataGridIssue.KeyDown          += new System.Windows.Forms.KeyEventHandler(this.DeleteRowDataGrid);

            /**
             * ButtonFetchIssue
             */
            this.ButtonFetchIssue.Location = new System.Drawing.Point(515, 255);
            this.ButtonFetchIssue.Size     = new System.Drawing.Size (70 , 30 );
            this.ButtonFetchIssue.Name     = "ButtonFetchIssue";
            this.ButtonFetchIssue.Text     = "Get Issue"       ;
            this.ButtonFetchIssue.Click += new System.EventHandler(this.FetchIssue);

            /**
             *  ButtonPrintIssue
             */
            this.ButtonPrintIssue.Location = new System.Drawing.Point(600, 255);
            this.ButtonPrintIssue.Size     = new System.Drawing.Size (70 , 30 );
            this.ButtonPrintIssue.Name     = "ButtonPrintIssue";
            this.ButtonPrintIssue.Text     = "Print Issue"     ;
            this.ButtonPrintIssue.Click += new System.EventHandler(this.PrintIssue);

            /**
             * ComboBoxPrinter
             */
            this.ComboBoxPrinter.Size     = new System.Drawing.Size (155, 20 );
            this.ComboBoxPrinter.Location = new System.Drawing.Point(515, 290);
            this.ComboBoxPrinter.Name     = "ComboBoxPrinter";
            this.ComboBoxPrinter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxPrinter.SelectionChangeCommitted += new System.EventHandler(this.SelectPrinter);

            this.ClientSize = new System.Drawing.Size(685, 325);
            this.Name       = "MainForm"   ;
            this.Text       = "Print Issue";

            this.Controls.Add(this.WebBrowser      );
            this.Controls.Add(this.Label           );
            this.Controls.Add(this.DataGridIssue   );
            this.Controls.Add(this.ButtonPrintIssue);
            this.Controls.Add(this.ButtonFetchIssue);
            this.Controls.Add(this.ComboBoxPrinter );
            this.MaximizeBox = false;

            this.ResumeLayout(false);
            this.PerformLayout();

            ((System.ComponentModel.ISupportInitialize)(this.DataGridIssue)).EndInit();
        }
    }
}