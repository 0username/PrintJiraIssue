﻿using System.Collections.Generic;

using JIRAIssue.Library.Print;

namespace JIRAIssue.App.View
{
    public delegate void PrintEventHandler(string arg);
    public delegate void IssueEventHandler(List<string> IDIssue);

    public interface IMainForm
    {
        void InitPresenter();

        event PrintEventHandler EventSetPrinter;
        event PrintEventHandler EventGetPrinter;
        event IssueEventHandler EventFetchIssue;

        void SetPrinter(List<Printer> printer);

        void ShowIssue(string issue);
    }
}