﻿using System;
using System.Collections.Generic;

using JIRAIssue.App.Model;
using JIRAIssue.App.View;

using JIRAIssue.Library.Print;
using JIRAIssue.Library.Ticket;

namespace JIRAIssue
{
    class Presenter
    {
        private IJiraApi Model;
        private IMainForm View;

        public Presenter(IJiraApi model, IMainForm view)
        {
            Model = model;
            View  = view;

            View.EventSetPrinter += new PrintEventHandler(SetPrinter);
            View.EventGetPrinter += new PrintEventHandler(GetPrinter);
            View.EventFetchIssue += new IssueEventHandler(FetchIssue);

            View.InitPresenter();
        }

        private void SetPrinter(string arg)
        {
            Print.SetDefaultPrinter(arg);
        }

        private void GetPrinter(string arg)
        {
            View.SetPrinter(Print.GetPrinter());
        }

        public async void FetchIssue(List<string> arg)
        {
            try
            {
                View.ShowIssue(Ticket.GetTicket(await Model.GetIssueAsync(arg)));
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.TargetSite + ": " + exception.Message);
            }
        }
    }
}