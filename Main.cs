﻿using System;
using System.Windows.Forms;
using System.Configuration;

using JIRAIssue.App.Model;
using JIRAIssue.App.View;

using JIRAIssue.Library.Print;

namespace JIRAIssue
{
    public static class _Main
    {
        [STAThread]
        public static void Main()
        {
            ResetDefaultPrinter(false);

            AppDomain.CurrentDomain.ProcessExit += new EventHandler(AppClosing);

            String jiraServer   = ConfigurationManager.AppSettings["jiraServer"];
            String jiraUsername = ConfigurationManager.AppSettings["jiraUsername"];
            String jiraPassword = ConfigurationManager.AppSettings["jiraPassword"];

            JiraApi model       = new JiraApi  (jiraServer, jiraUsername, jiraPassword);
            MainForm view       = new MainForm ();
            Presenter presenter = new Presenter(model, view);

            Application.Run(view);
        }

        private static void ResetDefaultPrinter(bool flag)
        {
            switch (flag)
            {
                case true  : Properties.Settings.Default.ProgramPrinter = Print.GetPrinter()[0].Name;
                    break;
                case false : Properties.Settings.Default.DefaultPrinter = Print.GetPrinter()[0].Name;
                    break;
            }

            Properties.Settings.Default.Save();

            Print.SetDefaultPrinter(((flag) ? Properties.Settings.Default.DefaultPrinter : Properties.Settings.Default.ProgramPrinter));
        }

        private static void AppClosing(object sender, EventArgs args)
        {
            ResetDefaultPrinter(true);
        }
    }
}