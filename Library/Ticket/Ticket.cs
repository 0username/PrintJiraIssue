﻿using System.Collections.Generic;
using System.Linq;

using HtmlAgilityPack;
using System;
using JIRAIssue.App.Model;

namespace JIRAIssue.Library.Ticket
{
    public static class Ticket
    {
        public static string GetTicket(List<JiraIssue> issue)
        {
            HtmlDocument document = new HtmlDocument();

            document.Load(new Uri(System.Windows.Forms.Application.StartupPath + Properties.Settings.Default.PathTicket).LocalPath);

            var table = document.GetElementbyId("ticket");

            table.Remove();

            foreach (var item in issue)
            {
                document.DocumentNode.InnerHtml += FillTicket(table, item);
            }

            return document.DocumentNode.InnerHtml;
        }

        private static string FillTicket(HtmlNode node, JiraIssue issue)
        {
            IEnumerable<HtmlNode> elements = from   span in node.Descendants("span")
                                             where  span.Id != ""
                                             select span;

            elements.Select(element => { element.InnerHtml = GetFieldValue(issue, element.Id); return element; }).ToList();

            return node.OuterHtml;
        }

        private static string GetFieldValue(object obj, string name)
        {
            return (string)obj.GetType      ().
                               GetProperties().
                               Single(field => field.Name == name).GetValue(obj, null);
        }
    }
}