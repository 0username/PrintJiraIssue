﻿using System;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Management;
using System.Linq;

namespace JIRAIssue.Library.Print
{
    static class Print
    {
        [DllImport("winspool.drv", CharSet = CharSet.Auto, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern Boolean SetDefaultPrinter(String name);

        public static List<Printer> GetPrinter()
        {
            List<Printer> list = new List<Printer>();

            var printers = new ManagementObjectSearcher("SELECT * FROM Win32_Printer");

            foreach (ManagementObject printer in printers.Get())
            {
                list.Add(new Printer
                {
                    Name     = ((string)printer.GetPropertyValue("name")).Split(new[] { "\\" }, StringSplitOptions.None).Last(),
                    FullName =  (string)printer.GetPropertyValue("name"),
                    Default  =  (bool)  printer.GetPropertyValue("default")
                });

            }

            return list.OrderBy(field => field.Default ? 0 : 1).ToList();
        }
    }
}