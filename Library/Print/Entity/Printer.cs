﻿using System;

namespace JIRAIssue.Library.Print
{
    public class Printer
    {
        public String Name     { get; set; }
        public String FullName { get; set; }
        public Boolean Default { get; set; }
    }
}